// Mathematical Operators

/*
 
 + sum
 - Difference
 -Product
 / qoutient
% Remainder


used when the value of the first number is stored in a variable; performs the operation
 += addition
 -=subtraction
 *=multiplication
 /=division
 %=Modulo

 */

function mod() {
	//return 9+2
	//return 9-2
	//return 9*2
	//return 9/2
	//return 9%2

	let x = 10
	//return x += 2
	//return x -= 2
	//return x *= 2
	//return x /= 2
	return x %= 2
};


console.log(mod());


// Assignment Operator (=) is used for asisigning values to variables

let x = 1;
let sum = 1;
//sum += 1;

x -= 1;
sum += 1;
console.log(sum);
console.log(x);


// Increment & Decrement



//Pre-imcrement - add 1 BEFORE assigning the value to the variable

let z = 1;
//let z = (1) + 4;
let increment = ++z;

console.log (`Result of pre-increment: ${increment}`);
console.log(`Result of pre-increment: ${z}`);


//Post-increment - adding 1 AFTER assigning the value to the variable

increment = z++;
// let z = 1 + (1);
 console.log(`Result of pre-increment: ${increment}`);
 console.log(`Result of pre-increment: ${z}`);


//Decrement


//pre-decrement - subtracts 1 BEFORE the assigning of values (basing from the mon=st recent value of z)

let decrement = --z;
console.log(`Result of pre-decrement: ${decrement}`);
console.log(`Result of pre-decrement: ${z}`);



//Post-decrement subtracting 1 AFTER assigning of values
decrement = z--;

console.log(`Result of pre-decrement: ${decrement}`);
console.log(`Result of pre-decrement: ${z}`);


//Comparison Operators

//Equality Operator (==)
let juan = "juan";


console.log( 1 == 1 );
console.log( 0 == false );
console.log(juan == "juan");

//Strcit Equality (===)
// use case when navigating in the webpages course, course outline
/*if (course == Course) {
	course.html
}*/

/*if (Data == Course Outline) {
	courseOutline.html
}*/


// the javascript is not anymore concerned with other relative functions of the data, it only compares the two values as they are;
console.log( 1 === true );
console.log(juan === "Juan");

//inequality ( != )
console.log (1 != 1);
console.log(juan != "juan");



//strictly inequality (!==)
console.log( 0 !== false );

//Other Comparison Operators

/*
 > Greater than
 < less than
 >= greater than or equal
 <= less than or equal
 */

 //Logical Operators


/*And Operator (&&) - returns true if all operands are true; 
true + true = true
true + false = false
false + true = false
*/
let isLegalAge = true;
let isRegistered = true;

let allRequirementMet = isLegalAge && isRegistered;
console.log(`Result of logical AND operator: ${allRequirementMet}`);



/*
OR Operator (||) - returns if one of the operands are true;

true + true = true
true + false = true
false + true = true
false + false = false
*/
 allRequirementMet = isLegalAge || isRegistered;
console.log(`Result of logical OR Operator: ${allRequirementMet}`)



//Selection Control Structures

/*

IF Statements - executes a command if a specified condition is true

	SYNTAX:
	if (condition) {
	statement/ command
	}
*/

let num = -1;

if (num<0) {
	console.log("Hello")
}

let value = 30;
if (value>10 && value<40) {
	console.log(`Welcome to Zuitt`)
}

/*
if-else statement - executes a command if the first condition returns false

if (condition) {
	statement if true
}

else{
	statement is false
};
*/


num = 5
if (num>10) {
	console.log(`Number is greater than 10`)
}
else {
	console.log(`Number is less than 10`)
};


/*
prompt - dialog box that has input fields


example:

num = prompt ("Please input a number.");

if (num>59) {
	console.log("Senior Age")
}
else {
	console.log("Invalid Age")
};



alert -dialog box that has no input fields used for warnings, announcements, etc.

number will be treated as String
example
num = prompt ("Please input a number.");

if (num>59) {
	alert("Senior Age")
	console.log(num)
}
else {
	alert("Invalid Age")
	console.log(num)
};

parseInt - converts string data type into numerical data types; cannot convert a boolean; only recognizes numbers and ignores alphabets
NaN- in the console means not a number
 example

num = parseInt(prompt("Please input a number."));

if (num>59) {
	alert("Senior Age")
	console.log(num)
}
else {
	alert("Invalid Age")
	console.log(num)
};

*/


//if-elseif-else statement


/*
1. Quezon
2. Valenzuela
3. Pasig
4. Taguig




let city = parseInt(prompt("Enter a Number"));
if (city === 1) {
	alert("Welcome to Quezon City")
}
else if (city === 2) {	
alert("Welcome to Valenzuela City")
}

else if (city === 3) {	
alert("Welcome to Pasig City")
}

else if (city === 4) {	
alert("Welcome to Taguig City")
}

else {
	alert("Invalid Number")
};
*/

let message = "";

function determineTyphoonIntensity(windspeed){
	if(windspeed < 30){
		return	`Not a typhoon yet.`
	}
	else if (windspeed <= 61) {
		return `Tropical Depression detected.`
	}
	else if (windspeed >=62 && windspeed <= 88){
		return `Tropical Storm detected.`
	}
	else if (windspeed >= 89 && windspeed <= 117) {
		return	`Severe Tropical Storm detected`
	}
	else{
		return `Typhoon detected`
	}
};
message = determineTyphoonIntensity(120);
console.log(message);


/*
Ternary Operator- short handed if-else statement

SYNTAX:
(condition) ? ifTrue: ifFalse
*/

let ternaryResult = (1 < 18) ? true:false
console.log(`Result of ternary operator: ${ternaryResult}`);


let name;
function isOfLegalAge(){
	name = "John";
	return "You are of the legal age Limit"
};

function isUnderAge(){
	name = "Jane";
	return "You are under the legal Age"
};
/*let age = parseInt(prompt("What is Your Age?"));
let LegalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();

alert( `Result of Ternary Operator in Function: ${LegalAge} ${name}` );
*/

// Switch Statement - a shorthand for if-elseif-else should there be a lot of conditions to be met

// break- signals the browser

/*let day = prompt("What day is is today?").toLowerCase();
switch (day) {
	case "sunday":
		alert("The color of the day is red");
		break;
	case "monday":
		alert("The color of the day is orange");
		break;
	case "tuesday":
		alert("The color of the day is yellow");
		break;
	case "wednesday":
		alert("The color of the day is green");
		break;
	case "thursday":
		alert("The color of the day is blue");
		break;
	case "friday":
		alert("The color of the day is violet");
		break;
	case "saturday":
		alert("The color of the day is indigo");
		break;
	default:
		alert("Please input a valid day");
};

*/
// Try-Catch-Finally Statement- commonly used for error handling

function showIntentsityAlert(windspeed){
	try{
		alert(determineTyphoonIntensity(windspeed));
	}
	catch(error){
		console.log(typeof error);
		console.warn(error.message);
	}
	finally{
		alert("Intensity updates will show new alert")
	}
}
showIntentsityAlert(56);